# Mongo Interview

This is to test your familiarity with databases and querying, and Mongo in particular. The Schema comes first, and the questions are at the end.

# Schema 

This is the schema for database `testdb`. Ues [remakte_test_interview_db.py](https://gist.github.com/csaftoiu/8f7ef7036a698910243d13c0fb700d78) script to create the database that can then be queried.

## Collection `blocks`

The `blocks` collection represents a linked list of blocks. The next link in the list is the block whose `prev_id` is equal to the current block id.

### Fields:

* **_id**: `string`, the unique block id
* **prev_id**: `_id?`, refers to the previous block. `None` if this is the first block.

## Collection `transactions`

Transactions are placed on blocks. Each document in `transactions` contains the `block_id` it's on.

Transactions have a `foo` value. This is very important. We want to know what the biggest `foo` is.

### Fields:

* **_id**: `string`, the unique transaction id
* **on_block_id**: `string?`, block id of the block the transaction is on, or None if not on a block.
* **foo**: `int`, the transaction's `foo` value (very important!)

# Question 1

Write a function, `get_biggest_foo_on_block(block_id)`. This returns the biggest `foo` value for all the transactions on the given block. If the block does not exist, or does not have any transactions on it, raise a ValueError().

## Test Cases

        assert get_biggest_foo_on_block(db, 'zudaqyoeifkhxf') == 87496
        assert get_biggest_foo_on_block(db, 'cwuongmqavu') == 83106
        assert get_biggest_foo_on_block(db, 'upqbtjs') == 4242560329
    
        try:
            print get_biggest_foo_on_block(db, 'tlunclbyyiitf')
            raise RuntimeError("Should have thrown value error")
        except ValueError:
            pass

# Question 2

Write a function, `count_transactions_until_end(block_id)`.

This takes a `block_id`, and returns the number of transactions on that block, and on every block until the end of the blockchain (until there are no blocks whose `prev_id` is equal to the current `block_id`). 

If the block does not exist, raise a ValueError().

## Test Cases

        assert count_transactions_until_end(db, 'jdezobkfbyeohh') == 24
        assert count_transactions_until_end(db, 'imdgclrljvhqrhg') == 12
        assert count_transactions_until_end(db, 'mocexgqtln') == 1
    
        try:
            print count_transactions_until_end(db, 'not a block id')
            raise RuntimeError("Should have thrown value error")
        except ValueError:
            pass


