#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Imports
import pywrapper_config
from pymongo import MongoClient,ASCENDING, DESCENDING


class TestPyMongo(object):
    def __init__(self,configFile):
        self.__config = pywrapper_config.Config(configFile)
        self.__host = self.__config.show_value_item("server","host")
        self.__port = int(self.__config.show_value_item("server","port"))
        self.__dbName = self.__config.show_value_item("database","name")
        self.__collections = self.__config.show_value_item("database","collections").split(",")

        self.__client = MongoClient(self.__host, self.__port)
        self.__db = self.__client[self.__dbName]
        self.__collectionTransactions = self.__db[self.__collections[1]]
        self.__collectionBlocks = self.__db[self.__collections[0]]



    def get_biggest_foo_on_block(self,block_id):

        query = self.__collectionTransactions.find({'on_block_id': block_id}).sort(u'foo',DESCENDING)
        for cursor in query:
            return cursor[u'foo']



    def count_transactions_until_end(self,block_id):
        count = 0
        query1 = self.__collectionBlocks.find_one({'_id': block_id})
        print query1
        count += int(self.__collectionTransactions.find({'on_block_id': query1[u'_id']}).count())
        next_id = query1[u'prev_id']
        while True:
            try:
                if query1[u'prev_id'] == None: break
                query1 = self.__collectionBlocks.find_one({'_id': next_id})
                print query1
                count += int(self.__collectionTransactions.find({'on_block_id': query1[u'_id']}).count())
                next_id = query1[u'prev_id']
            except (TypeError):
                break
        return count






if __name__ == "__main__":
    testMongoDB = TestPyMongo("../config.cnf")
    #print testMongoDB.get_biggest_foo_on_block('zudaqyoeifkhxf')
    #print testMongoDB.get_biggest_foo_on_block('cwuongmqavu')
    #print testMongoDB.get_biggest_foo_on_block('upqbtjs')
    #print testMongoDB.count_transactions_until_end('jdezobkfbyeoh')
    #print testMongoDB.count_transactions_until_end('imdgclrljvhqrhg')
    #print testMongoDB.count_transactions_until_end('mocexgqtln')
    print testMongoDB.count_transactions_until_end("mocexgqtln")
    #print testMongoDB.count_transactions_until_end("jdezobkfbyeohh")
    #print testMongoDB.count_transactions_until_end("")
